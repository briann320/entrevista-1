import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import { Bill } from '../products/product';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  bill: Bill = {
    id: '1',
    name: 'total acount',
    price: 0
  };
  selectedItems = []
  selectedProduct;
  val;
  offer;
  total;

  constructor(
    private cartService: CartService
  ) { }

  ngOnInit() {
    console.log('esto es servicio', this.selectedItems);
  }
  
  ngDoCheck() {
    this.selectedItems = this.cartService.getProducts();
    console.log(' productos que tengo', this.selectedItems)
    this.offer = this.cartService.changePrice(this.selectedItems);
    console.log('salsa de carne ', this.offer)
    this.selectedItems = this.offer;
    console.log('salsa de carne ', this.selectedItems)
  }

  totalBill(){
    console.log(this.bill)
    this.bill.price =0;
    for(let i = 0; i<this.selectedItems.length;i++){
      this.bill.price +=this.selectedItems[i].totalPrice;
    }
    console.log("totalisimo", this.bill.price)
    this.total= this.bill.price;
 }

  add(product) {
    /* if (this.val >= 0) {
      this.val++;
    } */
    product.demamd++;

  }
  deleteItem(object) {

    if (object.demand < 0) {
      return
    } else if (object.demand == 0) {
      this.selectedItems.map((currElement, index) => {
        if (currElement.id == object.id) {
          this.selectedItems.splice(index, 1);
          console.log(object);

        }
        console.log(object);
      });
      return
    } else {
      object.demand--;

    }

  }
  deleteAll() {

    this.selectedItems=[];
    
console.log(this.selectedItems)
  } 
}