import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductsComponent } from './products.component';
import {ProductsResolver} from './products.resolver';
import { AppComponent } from '../app.component';
const routes: Routes = [
    {
      path: '',
      component: ProductsComponent,
      resolve: {
        data: ProductsResolver
      }
    }
  ];
@NgModule({
  declarations: [
      AppComponent
    
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule
    
  ],
  providers: [ProductsResolver],
  bootstrap: [AppComponent]

})
export class AppModule { }