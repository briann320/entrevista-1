import { Component, OnInit } from '@angular/core';
import { Product } from './product';
import { PRODUCTS } from './mock-products';
import { CartService } from '../services/cart.service';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products = PRODUCTS;
  selectedProduct;
  val = 0;
 

  constructor(
    private cartService: CartService
  ) { }


  ngOnInit() {
    /* cosa */
    /* this.selectedProduct = this.cartService.addObject(); */
  }

  /*   objectSelected() {
      if (this.value <= 0) { return }
      else if (this.value > 0) {
        this.cartService.addObject(this.selectedProduct);
       console.log(this.selectedProduct);
    }
   */
  /*  addOffers(selectedItems){
     if(selectedItems.id=='GR1'){
 
     } 
 
 }
}*/
  add(p) {
    p.demand++;
    console.log("Producto!!", p);
    /* if (this.val >= 0) {
     this.val++;
    } */

  }
  remove(p) {
    if (p.demand <= 0) { return }
    else if (p.demand > 0) {
      p.demand--;
    }
  }

  addToCart(prodSelect, val) {
    if (val <= 0) { return }
    else if (val > 0) {
      this.selectedProduct=this.cartService.addObject(prodSelect);
      /* prodSelect.demand += val; */
    }
    console.log(this.selectedProduct);
  }
  deleteToCart(prodSelect, val) {
    if (val <= 0) { return }
    else if (val > 0) {
      this.cartService.removeObject(prodSelect);
      prodSelect.demand -= val;
    }

    console.log(this.selectedProduct);
  }
}

