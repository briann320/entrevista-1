import { Product } from './product';

export const PRODUCTS: Product[]=[
{
    id:'GR1', 
name:'Green tea', 
price:3.11,
pricesecondary:1.55,
totalPrice:0,image: `https://cdn.pixabay.com/photo/2017/08/02/19/46/green-tea-2573082_960_720.jpg`, 
alt:' photo of  green tea',
demand:0
},
{
    id:'SR1',
     name:'Strawberries',
      price:5.00,
      pricesecondary:4.50,
      totalPrice:0,
      image:`https://cdn.pixabay.com/photo/2016/02/04/22/45/strawberry-1180048_960_720.jpg`,
      alt:'Photo of fruits',
      demand:0
    },
{
    id:'CF1', 
    name:'Coffee', 
    price:11.23,
    pricesecondary:7.48,
    totalPrice:0, 
    image:`https://cdn.pixabay.com/photo/2018/12/03/21/40/cappuccino-3854326_960_720.jpg`, 
    alt:'photo of coffee',
    demand:0
}

] 

