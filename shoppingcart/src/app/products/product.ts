export class Product{
        id:string;
        name:string;
        price:number; 
        pricesecondary:number; 
        totalPrice:any;
        image:string;
        alt: string;
        demand: number;
    
}
export class Bill{
        id:string;
        name:string;
        price:number;  
}