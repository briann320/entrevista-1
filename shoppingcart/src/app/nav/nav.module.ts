import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
/* import { MatSliderModule, MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, 
    MatSortModule, MatTableModule,MatCardModule} from '@angular/material';
 */
import { NavComponent } from './nav.component';
import {NavResolver} from './nav.resolver';
import { AppComponent } from '../app.component';
import { ProductsComponent } from '../products/products.component';


const routes: Routes = [
    {
      path: '',
      component: NavComponent,
      resolve: {
        data: NavResolver
      }
    }
  ];
@NgModule({
  declarations: [
      AppComponent,
      ProductsComponent
      
    
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    
    
  ],
  providers: [NavResolver],
  bootstrap: [AppComponent]

})
export class AppModule { }